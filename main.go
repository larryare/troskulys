package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {

	var maziau int = 0
	var daugiau int = 0

	for i := 0; i < 1000; i++ {
		go RandomNumber(&maziau, &daugiau)
		spook := [3]int{maziau, daugiau, i}
		plt(spook, "To alaus or not to alaus?")
	}
	if daugiau > maziau {
		fmt.Println("Eik į Norfa senelyzai")
	} else {
		fmt.Println("Virkis arbatą")
	}
}

func RandomNumber(maziau *int, daugiau *int) {
	s1 := rand.NewSource(time.Now().UnixNano())
	r2 := rand.New(s1)
	result := r2.Intn(100)
	if result <= 50 {
		*maziau++
	} else {
		*daugiau++
	}
}
