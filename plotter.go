package main

import (
	"fmt"
	"os"
	"strconv"

	"github.com/go-echarts/go-echarts/v2/charts"
	"github.com/go-echarts/go-echarts/v2/opts"
)

// // generate random data for bar chart
// func generateBarItems() []opts.BarData {
// 	items := make([]opts.BarData, 0)
// 	for i := 0; i < 2; i++ {
// 		items = append(items, opts.BarData{Value: rand.Intn(300)})
// 	}
// 	return items
// }

func plt(values [3]int, title string) {
	items := make([]opts.BarData, 0)
	items = append(items, opts.BarData{Value: values[0]}, opts.BarData{Value: values[1]})
	fname := fmt.Sprintf("images/%v.html", values[2])
	// create a new bar instance
	bar := charts.NewBar()
	// set some global options like Title/Legend/ToolTip or anything else
	bar.SetGlobalOptions(charts.WithTitleOpts(opts.Title{
		Title:    strconv.Itoa(values[2]),
		Subtitle: title,
	}))

	// Put data into instance
	bar.SetXAxis([]string{"NOPE", "YES"}).
		AddSeries("Category A", items)
	// Where the magic happens
	f, _ := os.Create(fname)
	bar.Render(f)
}
