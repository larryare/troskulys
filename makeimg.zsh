for file in images/*(.)
do
    #echo img/$file:t:r.png
    brave --no-sandbox --headless --disable-gpu --screenshot=images/img/$file:t:r.png --window-size=1280x700 $file
done
